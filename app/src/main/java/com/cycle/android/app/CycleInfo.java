package com.cycle.android.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class CycleInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cycle_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        int like =DummyData.dataList.get(getIntent().getIntExtra("position", 0)).like ;
        int comments =DummyData.dataList.get(getIntent().getIntExtra("position", 0)).comments ;
        ((TextView)findViewById(R.id.textView)).setText(like+ " Likes");
        ((TextView)findViewById(R.id.textView2)).setText(comments+ " Comments");
        (findViewById(R.id.linearLayout)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CycleInfo.this, LikeComments.class);
                intent.putExtra("position",getIntent().getIntExtra("position", 0));
                startActivity(intent);
            }
        });

    }
}
