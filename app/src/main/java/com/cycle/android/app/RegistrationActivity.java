package com.cycle.android.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by jitendra.karma on 23/04/2016.
 */
public class RegistrationActivity extends Activity implements View.OnClickListener {

    private boolean isOTPRecieved = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_registration);
        isOTPRecieved = false;

        findViewById(R.id.send_otp_btn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_otp_btn:
                if (!isOTPRecieved){
                    findViewById(R.id.enterotp).setVisibility(View.VISIBLE);
                    isOTPRecieved = true;
                    ((Button)findViewById(R.id.send_otp_btn)).setText("Enter OTP");
                }else{
                    startActivity(new Intent(this, LoginActivity.class));
                }

                break;
        }
    }
}
