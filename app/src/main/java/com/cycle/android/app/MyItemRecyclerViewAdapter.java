package com.cycle.android.app;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.List;


public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private final List<Data> mValues;

    public MyItemRecyclerViewAdapter(List<Data> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.image.setImageResource(mValues.get(position).drawable);
        holder.name.setText(mValues.get(position).name);
        holder.department.setText(mValues.get(position).department);


    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView image;
        public final TextView name;
        public final TextView department;
        public Data mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            this.image = (ImageView) view.findViewById(R.id.pic);;
            name = (TextView) view.findViewById(R.id.cycle);
            department = (TextView) view.findViewById(R.id.name);
            view.findViewById(R.id.hire).setVisibility(View.GONE);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + name.getText() + "'";
        }
    }
}
