package com.cycle.android.app;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

/**
 * Created by rankushkumar.a on 27-04-2016.
 */
public class MainAdapter implements ListAdapter {

    private Context  mContext;

    public MainAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return DummyData.dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null ){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.main_list_item,null);
        }

        ImageView image = (ImageView) convertView.findViewById(R.id.pic);
        image.setImageResource(DummyData.dataList.get(position).drawable);

        TextView name = (TextView) convertView.findViewById(R.id.name);
        name.setText(DummyData.dataList.get(position).name);



        Button hire = (Button) convertView.findViewById(R.id.hire);
        hire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CycleInfo.class);
                intent.putExtra("position",position);
                mContext.startActivity(intent);
            }
        });

        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return DummyData.dataList.size();
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
