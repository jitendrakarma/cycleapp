package com.cycle.android.app;

/**
 * Created by rankushkumar.a on 27-04-2016.
 */
public class Data {

    public int drawable ;
    public String name;
    public int like;
    public int comments;
    public String department;




    public Data(int drawable, String name, int like, int comments, String department) {
        this.drawable = drawable;
        this.name = name;
        this.like = like;
        this.comments = comments;
        this.department = department;
    }
}
