package com.cycle.android.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

/**
 * Created by jitendra.karma on 23/04/2016.
 */
public class LoginActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);

        findViewById(R.id.forgot_password_text).setOnClickListener(this);
        findViewById(R.id.sign_up_text).setOnClickListener(this);
        findViewById(R.id.sign_in_btn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.forgot_password_text:
                Toast.makeText(this, "Forgot Password click", Toast.LENGTH_SHORT).show();
                break;
            case R.id.sign_in_btn:
                startActivity(new Intent(this, MainActivity.class));
                break;
            case R.id.sign_up_text:
                Intent intent = new Intent(this, RegistrationActivity.class);
                startActivity(intent);
                break;
        }
    }
}
