package com.cycle.android.app;

import java.util.ArrayList;

/**
 * Created by rankushkumar.a on 27-04-2016.
 */
public class DummyData {

    public static ArrayList<Data> dataList = new ArrayList<>();

    public static void init() {
        dataList.add(new Data(R.drawable._0000_layer_9, "Owner-1", 20, 45,"Department-1"));
        dataList.add(new Data(R.drawable._0006_layer_13, "Owner-2", 21, 53,"Department-2"));
        dataList.add(new Data(R.drawable._0007_layer_12, "Owner-3", 30, 46,"Department-3"));
        dataList.add(new Data(R.drawable._0008_layer_11, "Owner-4", 03, 50,"Department-4"));
        dataList.add(new Data(R.drawable._0009_layer_10, "Owner-5", 20, 55,"Department-5"));
        dataList.add(new Data(R.drawable._0014_layer_7, "Owner-6", 20, 20,"Department-6"));
        dataList.add(new Data(R.drawable._0016_layer_6, "Owner-7", 20, 20,"Department-7"));
        dataList.add(new Data(R.drawable._0017_layer_5, "Owner-8", 13, 40,"Department-8"));
        dataList.add(new Data(R.drawable._0018_layer_4, "Owner-9", 24, 60,"Department-1"));
        dataList.add(new Data(R.drawable._0019_layer_3, "Owner-10", 41, 60,"Department-2"));
        dataList.add(new Data(R.drawable._0020_layer_2, "Owner-11", 10, 05,"Department-3"));


    }

}
